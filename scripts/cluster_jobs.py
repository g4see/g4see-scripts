# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>

"""Submitting G4SEE parallel jobs on cluster for a single or parametric run, or delete submitted jobs."""

import os
import subprocess
import re
from copy import deepcopy
import multiprocessing
from pathlib import Path
import pandas as pd


def create_folder(path):
    """Create new subfolders."""
    try:
        os.mkdir(path)
    except OSError:
        if os.path.exists(path):
            pass
        else:
            raise OSError("Creation of the directory %s failed" % path)


def create_shell_script(filename, application, macro, folder, geant4_path):
    """Generate shell script for qsub."""
    script = open(filename, 'w')
    script.write("#!/bin/sh\n")
    if geant4_path is not None:
        script.write("source %s\n" % geant4_path)
    script.write("alias g4see='%s'\n" % application)
    script.write("g4see %s -o %s\n" % (macro, folder))
    script.close()
    return filename


def yaml_loader(filename):
    """Read yaml file."""
    import yaml
    with open(filename, 'r') as stream:
        try:
            p = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return p


def submit_g4see(script_path, queue_type):
    """Submit G4SEE job to cluster queue. Use absolute path here."""
    subprocess.call([
        'qsub', '-q', queue_type, script_path
    ])


def run_parameterized(processes, parent_folder, queue, geant4_path, g4see_path, macro_path=None, yaml_path=None):
    """Start G4SEE parameterized parallel jobs on cluster."""
    if geant4_path is not None:
        geant4_path = Path(geant4_path)
        print('Geant4 path:      %s' % geant4_path)
    else:
        print('Geant4 path:      None')
    g4see_path = Path(g4see_path)
    print('G4SEE app path:   %s' % g4see_path)

    if yaml_path is not None:
        parameters = yaml_loader(yaml_path)

        dirname = os.path.dirname(yaml_path)
        parameters['macro'] = os.path.join(dirname, parameters['macro'])
        with open(parameters['macro'], 'r') as fi:
            original_content = fi.readlines()

        for value_comb in parameters['value-list']:
            new_content = deepcopy(original_content)

            if not isinstance(value_comb, list):
                value_comb = [value_comb]
            value_comb = [str(v) for v in value_comb]

            cmdl = parameters['command-list']
            if not isinstance(cmdl, list):
                cmdl = [cmdl]

            k = 0
            for command_line in cmdl:
                a = command_line.split()

                for index, content_line in enumerate(new_content):
                    content_line = content_line.strip()
                    if content_line.startswith('#') or content_line == '':
                        continue
                    b = content_line.split('#', 1)[0].strip().split()   # ignoring end of line comments
                    if a == b:
                        if a.count('@') > 1:
                            for _ in range(a.count('@')):
                                new_content[index] = new_content[index].replace('@', value_comb[k], 1)
                                k += 1
                        elif a.count('@') == 1:
                            new_content[index] = new_content[index].replace('@', value_comb[k], 1)
                            k += 1
                        break

            if new_content == original_content:
                raise ValueError('No matching lines in yaml or mac file')

            values_str = []
            for vs in value_comb:
                if '/' in vs:
                    vs = vs[vs.rfind('/') + 1:]
                    vs = vs[:vs.rfind('.')]
                values_str += [vs]
            values_str = '_'.join(values_str).replace(' ', '')
            values_str = values_str.replace('/', '')
            values_str = values_str.replace('.', 'p')
            new_folder = parent_folder + '/' + values_str
            create_folder(new_folder)
            os.chdir(new_folder)

            new_macro = new_folder + '/param_' + values_str + '.mac'
            with open(new_macro, 'w') as fo:
                for line in new_content:
                    fo.write(line)

            run_multiprocessing(processes, parent_folder=new_folder, g4see=g4see_path, g4path=geant4_path,
                                macro=new_macro, queue=queue, jobname_str=values_str + '_')
    else:
        run_multiprocessing(processes, parent_folder=parent_folder, g4see=g4see_path, g4path=geant4_path,
                            macro=macro_path, queue=queue)


def run_multiprocessing(processes, parent_folder, g4see, g4path, macro, queue, jobname_str=''):
    """Start a G4SEE job on arbitrary number of cluster nodes in parallel."""
    for i in range(processes):
        pof = parent_folder + '/out_%d' % i
        create_folder(pof)
        os.chdir(pof)
        script = create_shell_script('g4see_%s%d.sh' % (jobname_str, i), g4see, macro, pof, g4path)
        if queue is not None:
            p = multiprocessing.Process(target=submit_g4see, args=(script, queue))
            p.start()
            p.join()


# def get_all_g4see_job_id(user):
#   """Get list of submitted G4SEE jobs of user."""
#   import re
#   g4see_job_regex = r'(\d+)(\.[a-z0-9. ]+)(g4see)'
#   proc = subprocess.Popen([
#       "qstat -u %s | grep -Po \"%s\"" % (user, g4see_job_regex)
#   ], shell=True, stdout=subprocess.PIPE)
#   stdout, _ = proc.communicate()
#   stdout = stdout.decode('UTF-8')
#   job_id_regex = r'\d+'
#   rx = re.compile(job_id_regex, re.VERBOSE)
#   return rx.findall(stdout)


def get_jobs(user):
    """Get details of all cluster jobs submitted by a user in a pandas DataFrame."""
    proc = subprocess.Popen([
        "qstat -f | grep %s@ -B 2 -A 9" % user
    ], shell=True, stdout=subprocess.PIPE)
    stdout, _ = proc.communicate()
    stdout = stdout.decode('UTF-8')
    df = pd.DataFrame()
    if len(stdout) == 0:
        return df
    job_attrs = ['Job_Name', 'job_state', 'queue', 'ctime',
                 'resources_used.cput', 'resources_used.mem', 'resources_used.vmem']
    dd = {}
    for line in stdout.splitlines():
        line = line.strip()
        if 'Job Id:' in line:
            rx = re.compile(r'\d+')
            dd['Job_ID'] = rx.findall(line)[0]
        for attr in job_attrs:
            if attr in line:
                dd[attr] = line.lstrip(attr + ' = ')
        if line == '--':
            df = df.append(dd, ignore_index=True)
            dd = {}
    df.rename(columns={'resources_used.cput': 'CPU_time', 'resources_used.mem': 'Memory',
                       'resources_used.vmem': 'VMemory', 'Job_Name': 'Job_name', 'job_state': 'Job_state',
                       'queue': 'Queue', 'ctime': 'Submit_time'}, inplace=True)
    df = df[['Job_ID', 'Job_name', 'Job_state', 'Queue', 'CPU_time', 'Memory', 'VMemory', 'Submit_time']]
    df['CPU_time'] = pd.to_timedelta(df['CPU_time'])
    df['Submit_time'] = pd.to_datetime(df['Submit_time'])
    df['Memory'] = df['Memory'].str.replace('kb', '').convert_dtypes(convert_string=False).astype(float)
    df['VMemory'] = df['VMemory'].str.replace('kb', '').convert_dtypes(convert_string=False).astype(float)
    df['Job_ID'] = df['Job_ID'].convert_dtypes(convert_string=False).astype(int)
    df['Job_name'] = df['Job_name'].convert_dtypes()
    df['Job_state'] = df['Job_state'].convert_dtypes()
    df['Queue'] = df['Queue'].convert_dtypes()
    return df


def filter_jobs(jobs, all_jobs=False, state=None, queue=None, name_substr=None, id_range=None):
    """Filter G4SEE jobs on cluster submitted by user."""
    jobs = jobs[jobs['Job_name'].str.contains('g4see')]     # discard non-G4SEE jobs of the user
    if all_jobs:
        return jobs
    criteria = []
    if state is not None:
        if state == 'R' or state == 'running':
            sel_sta = jobs['Job_state'] == 'R'
            criteria += [sel_sta]
        elif state == 'Q' or state == 'queue':
            sel_sta = jobs['Job_state'] == 'Q'
            criteria += [sel_sta]
    if queue is not None:
        sel_que = jobs['Queue'] == queue
        criteria += [sel_que]
    if name_substr is not None:
        sel_str = jobs['Job_name'].str.contains(name_substr)
        criteria += [sel_str]
    if id_range is not None:
        id_range = id_range[0]
        if len(id_range) == 1:
            sel_ran = (jobs['Job_ID'] >= id_range[0])
        else:
            sel_ran = (jobs['Job_ID'] >= id_range[0]) & (jobs['Job_ID'] <= id_range[1])
        criteria += [sel_ran]
    selected = pd.concat(criteria, axis=1).all(axis=1)
    return jobs.loc[selected]


def print_jobs(jobs_to_monitor, out):
    """Monitor jobs on cluster submitted by user."""
    if jobs_to_monitor.size > 0:
        with open(Path(out), 'w') as f:
            print(jobs_to_monitor.to_string(justify='center'), file=f)
    else:
        print('No jobs found to monitor.')


def delete_jobs(job_to_delete):
    """Delete jobs on cluster submitted by user."""
    if job_to_delete.size > 0:
        # print('qdel ' + ' '.join(job_to_delete['Job_ID'].astype(str)))
        proc = subprocess.Popen([
            'qdel ' + ' '.join(job_to_delete['Job_ID'].astype(str))
        ], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = proc.communicate()
        stderr = stderr.decode('UTF-8')
        if stderr == '':
            print('G4SEE jobs have been deleted.')
        else:
            print(stderr.split('\n')[0] + ' ...')
    else:
        print('No jobs found to delete.')
