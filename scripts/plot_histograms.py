# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>

"""Plotting G4SEE histograms from files."""

import os
from glob import glob
from pathlib import Path
import pandas as pd                 # type: ignore
import matplotlib.pyplot as plt     # type: ignore


def read_g4see_histogram(filename):
    """Read data from ascii file."""
    try:
        df = pd.read_table(filename, header=2, names=['Edep', 'Counts', 'StdDev'], sep=r'\s+')
        uof = pd.read_table(filename, header=0, names=['Edep', 'Counts', 'StdDev'], sep=r'\s+')
    except pd.errors.ParserError:
        df = pd.read_table(filename, header=2, names=['Edep', 'Counts'], sep=r'\s+')
        uof = pd.read_table(filename, header=0, names=['Edep', 'Counts'], sep=r'\s+')
    return df, uof['Counts'][0], uof['Counts'][1]


def plotter(path_str, hist: str = '', norm_factor: float = None, norm_bin: bool = False, output=None):
    """Plot histogram(s) of G4 simulation."""
    data_dictionary = {}
    path = Path(os.path.abspath(path_str))
    if path.is_dir():
        os.chdir(path)
        file_list = glob('**/' + hist + '_histogram.out', recursive=True)
        file_list.sort()
        # folder_list = [Path(*Path(p).parts[:-1]) for p in file_list]
        for k in file_list:
            data_dictionary[k] = os.path.abspath(k)
        title = 'G4SEE "' + hist + '" histogram(s) in path "' + path_str + '"'
    else:
        data_dictionary['data'] = path_str
        title = 'G4SEE histogram, "' + path_str + '"'

    # read data from files and plot
    plt.figure(figsize=(9, 6))
    data = None
    for label, filename in data_dictionary.items():
        data, _, _ = read_g4see_histogram(filename)
        if norm_bin:
            data['Counts'] /= (data['Edep'][1:].values - data['Edep'][:-1])  # normalize histograms by bin width
        if norm_factor:
            data['Counts'] /= norm_factor   # normalize histograms by fluence for example
        data.dropna(inplace=True)
        plt.hist(data['Edep'], bins=data['Edep'], weights=data['Counts'], histtype='step', label=label)

    # plot settings
    if round(data['Edep'][2]-data['Edep'][1], 8) != round(data['Edep'][3]-data['Edep'][2], 8):  # type: ignore
        plt.xscale('log')
    plt.yscale('log')
    plt.legend(loc='best')
    plt.title(title)
    ylabel = 'Counts'
    if norm_factor:
        ylabel += ' / Factor'
    if norm_bin:
        ylabel += ' / BinWidth'
    plt.xlabel('E (MeV)')
    plt.ylabel(ylabel)
    if output is not None:
        plt.savefig(output)
    plt.show()
