# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>

"""G4SEE geometry visualization from input macro file."""

import re
import os
from copy import deepcopy
import numpy as np                      # type: ignore
import matplotlib.pyplot as plt         # type: ignore
from matplotlib.path import Path        # type: ignore
import matplotlib.patches as patches    # type: ignore
from typing import Union


def bulk_vol(line: str):
    """Get parameters from Bulk macro command."""
    p = line.split()
    # command = p[0]
    return {
        'mat': str(p[1]),
        'width': lconv(float(p[2]), p[3]),
        'thick': lconv(float(p[4]), p[5]),
        # 'bias': p[6],
    }


def sensitive_vol(line: str):
    """Get parameters from SV macro command."""
    # /SEE/geometry/SV                0. 0. 0. um         10. 10. um  3. um       true
    p = line.split()
    # command = p[0]
    return {
        'pos': lconv(np.array([float(p[1]), float(p[2]), float(p[3])]), p[4]),
        'xwidth': lconv(float(p[5]), p[7]),
        'ywidth': lconv(float(p[6]), p[7]),
        'thick': lconv(float(p[8]), p[9]),
        # 'bias': p[10],
    }


def beol_vol(line: str):
    """Get parameters from BEOL macro command."""
    # /SEE/geometry/BEOL/addLayer     BEOL_mix            8. um       2. um       false   BeolMix
    p = line.split()
    # command = p[0]
    return {
        'mat': str(p[1]),
        'width': lconv(float(p[2]), p[3]),
        'thick': lconv(float(p[4]), p[5]),
        'name': str(p[6]),
        # 'bias': p[7],
    }


def lconv(num: Union[float, np.array], unit: str):
    """Get length in micrometers."""
    if unit == 'nm':
        factor = 1e-3
    elif unit == 'um':
        factor = 1.
    elif unit == 'mm':
        factor = 1e+3
    elif unit == 'cm':
        factor = 1e+4
    elif unit == 'm':
        factor = 1e+6
    else:
        raise ValueError('Unknown unit: `%s`' % unit)
    return num * factor


def get_length_in_um(line: str):
    """Get length in micrometers."""
    numeric_const_pattern = r'[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?'
    unit_pattern = r'nm | um | mm | cm | m'
    rx = re.compile(numeric_const_pattern, re.VERBOSE)
    ru = re.compile(unit_pattern, re.VERBOSE)
    unit = ru.findall(line)
    if unit == ['nm']:
        factor = 1e-3
    elif unit == ['um']:
        factor = 1.
    elif unit == ['mm']:
        factor = 1e+3
    elif unit == ['cm']:
        factor = 1e+4
    elif unit == ['m']:
        factor = 1e+6
    elif not unit:
        factor = 1.
    else:
        raise ValueError('Unknown unit: `%s`' % unit)
    lst = [float(i) * factor for i in rx.findall(line)]
    if len(lst) == 1:
        return lst[0]
    else:
        return lst


def get_boolean(line: str):
    """Get boolean value from Bulk macro command."""
    p = line.split()
    # command = p[0]
    try:
        if p[1] == 'true' or p[1] == 'True' or p[1] == '1':
            return True
        elif p[1] == 'false' or p[1] == 'False' or p[1] == '0':
            return False
        else:
            raise ValueError('Boolean value expected.')
    except IndexError:
        raise ValueError('Could not read boolean value from macro.')


def read_macro_dimensions(macro):
    """Read dimensions from G4SEE *.mac macro file."""
    gp = {}
    be = []
    sv_wm = False
    cyl = False
    with open(macro, 'r') as inp:
        for line in inp:
            line = line.strip()
            if line.startswith('#') or line == '':
                continue
            line = line.split('#', 1)[0]
            line = line.rstrip()

            if line.startswith('/SEE/geometry/Bulk'):
                bu = bulk_vol(line)
            elif line.startswith('/SEE/geometry/SV'):
                sv = sensitive_vol(line)
            elif line.startswith('/SEE/geometry/BEOL/addLayer'):
                be += [beol_vol(line)]
            # elif line.startswith('/gps/pos/centre'):
            #     gp['centre'] = get_length_in_um(line)
            # elif line.startswith('/gps/pos/halfx'):
            #     gp['halfx'] = get_length_in_um(line)
            # elif line.startswith('/gps/pos/halfy'):
            #     gp['halfy'] = get_length_in_um(line)
            # elif line.startswith('/gps/direction'):
            #     gp['dir'] = get_length_in_um(line)

            elif line.startswith('/SEE/geometry/setWorldAsMotherOfSV'):
                sv_wm = get_boolean(line)
            elif line.startswith('/SEE/geometry/setCylindricalGeometry'):
                cyl = get_boolean(line)

    return bu, sv, be, gp, sv_wm, cyl


def vertices(vp, p, wx=None, wy=None, wz=None):
    """Get vertices for patch."""
    if isinstance(p, np.ndarray):
        x, y, z = p[0], p[1], p[2]
    else:
        x, y, z = 0., 0., p
    if vp == 'xz':
        xy = x
        w = wx
        zz = z
        h = wz
    elif vp == 'yz':
        xy = y
        w = wy
        zz = z
        h = wz
    elif vp == 'xy':
        xy = x
        w = wx
        zz = y
        h = wy
    else:
        raise AttributeError
    return [
        (-0.5 * w + xy, -0.5 * h + zz),  # left, bottom
        (-0.5 * w + xy, 0.5 * h + zz),   # left, top
        (0.5 * w + xy, 0.5 * h + zz),    # right, top
        (0.5 * w + xy, -0.5 * h + zz),   # right, bottom
        (-0.5 * w + xy, -0.5 * h + zz),  # ignored
    ]


def visualize_geometry_xy(bulk: dict, sv: dict, beol_lst: list, gps: dict, world_sv_mother: bool,
                          view: str = 'xz', out=None, eq_ax=False):
    """Visualize geometry (from +X or +Y view direction)."""
    target_thickness_sum = 0.
    target_side_max = 0.
    for vol in beol_lst + [bulk]:
        target_thickness_sum += vol['thick']
        target_side_max = max(vol['width'], target_side_max)
    target_thickness_sum += sv['thick']
    target_side_max = max(sv['xwidth'], target_side_max)
    target_side_max = max(sv['ywidth'], target_side_max)
    world_xy = target_side_max / 2 * 1.5    # um
    world_z = target_thickness_sum * 1.5    # um
    verts_world = vertices(wx=world_xy*2, wy=world_xy*2, wz=world_z*2, p=0., vp=view)
    verts_void = vertices(wx=world_xy*100, wy=world_xy*100, wz=world_z*100, p=0., vp=view)

    verts_bulk = vertices(wx=bulk['width'], wy=bulk['width'], wz=bulk['thick'], p=-bulk['thick']/2, vp=view)

    sv_abs_pos = deepcopy(sv['pos'])
    sv_abs_pos[2] -= sv['thick'] / 2.     # absolute position needed
    verts_sv = vertices(wx=sv['xwidth'], wy=sv['ywidth'], wz=sv['thick'], p=sv_abs_pos, vp=view)

    codes = [Path.MOVETO,
             Path.LINETO,
             Path.LINETO,
             Path.LINETO,
             Path.CLOSEPOLY]

    fig = plt.figure()
    if eq_ax:
        ax = fig.add_subplot(111, aspect='equal')
    else:
        ax = fig.add_subplot(111)

    path_void = Path(verts_void, codes)
    patch_void = patches.PathPatch(path_void, alpha=0.3, facecolor='white', hatch='//')
    ax.add_patch(patch_void)

    path_world = Path(verts_world, codes)
    patch_world = patches.PathPatch(path_world, facecolor='white', lw=1, label='World')
    ax.add_patch(patch_world)

    path_bulk = Path(verts_bulk, codes)
    patch_bulk = patches.PathPatch(path_bulk, facecolor='C0', alpha=0.4, lw=1, label='Bulk')
    ax.add_patch(patch_bulk)

    if world_sv_mother:
        sv_col = 'white'
    else:
        sv_col = 'C0'
    path_sv = Path(verts_sv, codes)
    patch_sv = patches.PathPatch(path_sv, facecolor=sv_col, alpha=0.5, linestyle='--', lw=1, label='SV')
    ax.add_patch(patch_sv)

    prev_z_top = 0.
    i = 1
    for beol in beol_lst:
        z_center = prev_z_top + beol['thick']/2.
        prev_z_top += beol['thick']
        verts_beol = vertices(wx=beol['width'], wy=beol['width'], wz=beol['thick'], p=z_center, vp=view)
        path_beol = Path(verts_beol, codes)
        patch_beol = patches.PathPatch(path_beol, facecolor='C'+str(i), alpha=0.4, lw=1, label=beol['name'])
        ax.add_patch(patch_beol)
        i += 1

    ax.set_xlim(-0.55 * world_xy * 2, 0.55 * world_xy * 2)
    ax.set_ylim(-0.55 * world_z * 2, 0.55 * world_z * 2)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    if view == 'xz':
        plt.xlabel(r'X ($\mu$m)')
        plt.title('X-Z view')
    elif view == 'yz':
        plt.xlabel(r'Y ($\mu$m)')
        plt.title('Y-Z view')
    plt.ylabel(r'Z ($\mu$m)')

    # # arrows for beam
    # try:
    #     if view == 'yz':
    #         gpshw = gps['halfy']
    #     else:
    #         gpshw = gps['halfx']
    #     for vv in [-gpshw, gps['centre'][0], gpshw]:
    #         plt.annotate('', xytext=(vv, gps['centre'][2]), xy=(vv, -0.9 * world_z),
    #                      arrowprops=dict(arrowstyle='->', color='red', alpha=0.4))
    # except KeyError:
    #     try:
    #         plt.annotate('', xytext=(gps['centre'][0], gps['centre'][2]),
    #                      xy=(gps['centre'][0], -0.9 * world_z),
    #                      arrowprops=dict(arrowstyle='->', color='red', alpha=0.4))
    #     except KeyError:
    #         pass

    plt.tight_layout()
    if out is not None:
        filename, file_extension = os.path.splitext(out)
        plt.savefig(filename + '_' + view + file_extension)


def visualize_geometry_z(bulk: dict, sv: dict, beol_lst: list, gps: dict,
                         world_sv_mother: bool, cylindrical_geom: bool, out=None):
    """Visualize geometry (from +Z view direction)."""
    target_thickness_sum = 0.
    target_side_max = 0.
    for vol in beol_lst + [bulk]:
        target_thickness_sum += vol['thick']
        target_side_max = max(vol['width'], target_side_max)
    target_thickness_sum += sv['thick']
    target_side_max = max(sv['xwidth'], target_side_max)
    target_side_max = max(sv['ywidth'], target_side_max)
    world_xy = target_side_max / 2 * 1.5    # um
    world_z = target_thickness_sum * 1.5    # um
    verts_world = vertices(wx=world_xy*2, wy=world_xy*2, wz=world_z*2, p=0., vp='xy')
    verts_void = vertices(wx=world_xy*100, wy=world_xy*100, wz=world_z*100, p=0., vp='xy')

    verts_bulk = vertices(wx=bulk['width'], wy=bulk['width'], wz=bulk['thick'], p=-bulk['thick']/2, vp='xy')

    sv_abs_pos = deepcopy(sv['pos'])
    sv_abs_pos[2] -= sv['thick'] / 2.     # absolute position needed
    verts_sv = vertices(wx=sv['xwidth'], wy=sv['ywidth'], wz=sv['thick'], p=sv_abs_pos, vp='xy')

    codes = [Path.MOVETO,
             Path.LINETO,
             Path.LINETO,
             Path.LINETO,
             Path.CLOSEPOLY]

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    # ax = fig.add_subplot(111)

    path_void = Path(verts_void, codes)
    patch_void = patches.PathPatch(path_void, alpha=0.3, facecolor='white', hatch='//')
    ax.add_patch(patch_void)

    path_world = Path(verts_world, codes)
    patch_world = patches.PathPatch(path_world, facecolor='white', lw=1, label='World')
    ax.add_patch(patch_world)

    if cylindrical_geom:
        patch_bulk = patches.Circle((0, 0), radius=bulk['width'] / 2, color='C0', alpha=0.2, lw=1, label='Bulk')
    else:
        path_bulk = Path(verts_bulk, codes)
        patch_bulk = patches.PathPatch(path_bulk, facecolor='C0', alpha=0.2, lw=1, label='Bulk')
    ax.add_patch(patch_bulk)

    if world_sv_mother:
        sv_col = 'white'
    else:
        sv_col = 'C0'
    if cylindrical_geom:
        patch_sv = patches.Circle((sv['pos'][0], sv['pos'][1]), radius=sv['xwidth'] / 2, facecolor=sv_col,
                                  alpha=0.3, linestyle='--', lw=1, label='SV')
    else:
        path_sv = Path(verts_sv, codes)
        patch_sv = patches.PathPatch(path_sv, facecolor=sv_col, alpha=0.3, linestyle='--', lw=1, label='SV')
    ax.add_patch(patch_sv)

    prev_z_top = 0.
    i = 1
    for beol in beol_lst:
        z_center = prev_z_top + beol['thick']/2.
        prev_z_top += beol['thick']
        verts_beol = vertices(wx=beol['width'], wy=beol['width'], wz=beol['thick'], p=z_center, vp='xy')
        if cylindrical_geom:
            patch_beol = patches.Circle((0, 0), radius=beol['width'] / 2, facecolor='C'+str(i),
                                        alpha=0.2, lw=1, label=beol['name'])
        else:
            path_beol = Path(verts_beol, codes)
            patch_beol = patches.PathPatch(path_beol, facecolor='C'+str(i), alpha=0.2, lw=1, label=beol['name'])
        ax.add_patch(patch_beol)
        i += 1

    ax.set_xlim(-0.55 * world_xy * 2, 0.55 * world_xy * 2)
    ax.set_ylim(-0.55 * world_xy * 2, 0.55 * world_xy * 2)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xlabel(r'X ($\mu$m)')
    plt.ylabel(r'Y ($\mu$m)')
    plt.title('X-Y view')

    # try:
    #     for vv in [-gps['halfx'], gps['centre'][0], gps['halfx']]:
    #         for ww in [-gps['halfy'], gps['centre'][1], gps['halfy']]:
    #             cross = plt.scatter(vv, ww, c='red', marker='x', alpha=0.4)
    #             ax.add_artist(cross)
    # except KeyError:
    #     try:
    #         cross = plt.scatter(gps['centre'][0], gps['centre'][1], c='red', marker='x', alpha=0.4)
    #         ax.add_artist(cross)
    #     except KeyError:
    #         pass

    plt.tight_layout()
    if out is not None:
        filename, file_extension = os.path.splitext(out)
        plt.savefig(filename + '_xy' + file_extension)


def viewer(macro, output, show, equal):
    """Visualize geometry."""
    dbu, dsv, lbe, dgp, wsm, cg = read_macro_dimensions(macro)
    visualize_geometry_xy(dbu, dsv, lbe, dgp, wsm, view='xz', out=output, eq_ax=equal)
    visualize_geometry_xy(dbu, dsv, lbe, dgp, wsm, view='yz', out=output, eq_ax=equal)
    visualize_geometry_z(dbu, dsv, lbe, dgp, wsm, cg, out=output)
    if show:
        plt.show()
