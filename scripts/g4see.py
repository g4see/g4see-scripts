# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>

"""G4SEE scripts to submit or delete jobs, visualize geometry, merge or plot histograms."""

from os import path
import argparse
from cluster_jobs import run_parameterized                                  # noqa
from cluster_jobs import get_jobs, filter_jobs, delete_jobs, print_jobs     # noqa
from geometry_viewer import viewer                                          # noqa
from plot_histograms import plotter                                         # noqa
from recursive_merging import merger                                        # noqa
from cross_sections import cross_section                                     # noqa
from yaml import safe_load, YAMLError


def yaml_loader(filename):
    """Read yaml file."""
    with open(filename, 'r') as stream:
        try:
            p = safe_load(stream)
        except YAMLError as exc:
            print(exc)
    return p


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter, description=__doc__
    )
    subparsers = parser.add_subparsers(dest='subcommand')

    # subparser for submitting jobs
    parser_submit = subparsers.add_parser('submit', help='Submit jobs to cluster nodes')
    parser_submit.add_argument("input", type=str, help="Path of macro (.mac) or parametric YAML (.yaml) file ")
    parser_submit.add_argument("-o", "--output", type=str, required=True, help="Path of output folder")
    parser_submit.add_argument("-j", "--jobs", type=int, required=True, help="Number of parallel jobs to start")
    parser_submit.add_argument("-q", "--queue", type=str, default=None, help="Type of cluster queue")
    parser_submit.add_argument("-G4SEE", "--G4SEE_PATH", type=str, default='g4see',
                               help="Path to G4SEE application executable 'g4see'")
    parser_submit.add_argument("-G4", "--G4_PATH", type=str, default=None,  # /home/soft/geant/release/bin/geant4.sh
                               help="Path to the 'geant4.sh' file of Geant4 installation")

    # subparser for monitoring jobs
    parser_mon = subparsers.add_parser('monitor', help='Monitor G4SEE jobs submitted to cluster nodes')
    parser_mon.add_argument("user", type=str, help="Username")
    parser_mon.add_argument("-o", "--output", type=str, default='g4see_jobs.log',
                            help="Save job monitoring log in defined path")
    parser_mon.add_argument("-a", "--all", action='store_true', default=False, help="Monitor all G4SEE jobs")
    parser_mon.add_argument("-st", "--state", type=str, default=None,  choices=['R', 'running', 'Q', 'queue'],
                            help="Monitor G4SEE jobs in a specific state")
    parser_mon.add_argument("-q", "--queue", type=str, default=None, help="Monitor G4SEE jobs in a specific queue")
    parser_mon.add_argument("-ss", "--substring", type=str, default=None,
                            help="Monitor G4SEE jobs with a specific substring in their names")
    parser_mon.add_argument("-id", "--idrange", type=int, default=None, action='append', nargs='+',
                            metavar=('left', 'right'), help="Monitor G4SEE jobs within a Job ID range")

    # subparser for deleting jobs
    parser_del = subparsers.add_parser('delete', help='Delete G4SEE jobs submitted to cluster nodes')
    parser_del.add_argument("user", type=str, help="Username")
    parser_del.add_argument("-a", "--all", action='store_true', default=False, help="Delete all G4SEE jobs")
    parser_del.add_argument("-st", "--state", type=str, default=None, choices=['R', 'running', 'Q', 'queue'],
                            help="Delete G4SEE jobs in a specific state")
    parser_del.add_argument("-q", "--queue", type=str, default=None, help="Delete G4SEE jobs in a specific queue")
    parser_del.add_argument("-ss", "--substring", type=str, default=None,
                            help="Delete G4SEE jobs with a specific substring in their names")
    parser_del.add_argument("-id", "--idrange", type=int, default=None, action='append', nargs='+',
                            metavar=('left', 'right'), help="Delete G4SEE jobs within a Job ID range")

    # subparser for viewing geometry
    parser_view = subparsers.add_parser('view', help='Visualize geometry from macro file')
    parser_view.add_argument("macro", type=str, help="Path of G4SEE input macro file")
    parser_view.add_argument("-ns", "--no-show", dest='show', action='store_false', help="Show interactive figure")
    parser_view.set_defaults(show=True)
    parser_view.add_argument("-eq", "--equal", action='store_true', help="Figure with equal axes")
    parser_view.set_defaults(equal=False)
    parser_view.add_argument("-o", "--output", type=str, default='geometry.png', help="Save figure in defined path")

    # subparser for merging histograms
    parser_merge = subparsers.add_parser('merge', help='Merge histogram files recursively')
    parser_merge.add_argument("folder", type=str, help="Path of main data folder")

    # subparser for plotting histograms geometry
    parser_plot = subparsers.add_parser('plot', help='Plot histograms from files')
    parser_plot.add_argument("data", type=str, help="Path of histogram data file or folder with multiple histograms")
    parser_plot.add_argument("-ht", "--hist", type=str, default='Edep*',
                             help="Histogram type with id: Edep_1, Ekin_1, etc.")
    parser_plot.add_argument("-o", "--output", type=str, default='histogram.png', help="Save figure in defined path")
    parser_plot.add_argument("-nf", "--normFactor", type=float, default=None,
                             help="Normalize histograms by factor (e.g. fluence, number of primaries)")
    parser_plot.add_argument("-nb", "--normBin", action='store_true', help="Normalize histograms by bin width")
    parser_plot.set_defaults(normBin=False)

    # subparser for plotting histograms geometry
    parser_see_xs = subparsers.add_parser('see-xs', help='Calculate and plot SEE cross-sections')
    parser_see_xs.add_argument("config", type=str, help='Path of YAML config file')
    parser_see_xs.add_argument("-o", "--output", type=str, default='SEE_XS.png', help="Save figure in defined path")

    args = parser.parse_args()

    if hasattr(args, 'config') and args.config is not None:
        if not (args.config.endswith('.yml') or args.config.endswith('.yaml')):
            raise IOError('Unknown file format. Please use a YAML file with yml or yaml extension.')
        args.config = yaml_loader(path.abspath(args.config))

    if args.subcommand == 'submit':
        args.input = path.abspath(args.input)
        args.output = path.abspath(args.output)
        if not path.isdir(args.output):
            raise NotADirectoryError('No output directory: `%s`' % args.output)
        if not path.isfile(args.input):
            raise FileNotFoundError('Input file does not exist: `%s`' % args.input)
        if args.input.endswith('.yml') or args.input.endswith('.yaml'):
            run_parameterized(yaml_path=args.input, processes=args.jobs, parent_folder=args.output,
                              queue=args.queue, geant4_path=args.G4_PATH, g4see_path=args.G4SEE_PATH)
        elif args.input.endswith('.mac'):
            run_parameterized(macro_path=args.input, processes=args.jobs, parent_folder=args.output,
                              queue=args.queue, geant4_path=args.G4_PATH, g4see_path=args.G4SEE_PATH)
        else:
            raise IOError('Unknown file format')

    elif args.subcommand == 'monitor':
        args.output = path.abspath(args.output)
        if path.isdir(args.output):
            args.output = path.join(args.output, 'g4see_jobs.log')
        jobs = get_jobs(args.user)
        if jobs.size == 0:
            print('No jobs found to monitor.')
        else:
            selected_jobs = filter_jobs(jobs, all_jobs=args.all, state=args.state, queue=args.queue,
                                        name_substr=args.substring, id_range=args.idrange)
            if selected_jobs.size == 0:
                print('No jobs selected to monitor.')
            else:
                print_jobs(selected_jobs, args.output)

    elif args.subcommand == 'delete':
        jobs = get_jobs(args.user)
        if jobs.size == 0:
            print('No jobs found to delete.')
        else:
            selected_jobs = filter_jobs(jobs, all_jobs=args.all, state=args.state, queue=args.queue,
                                        name_substr=args.substring, id_range=args.idrange)
            if selected_jobs.size == 0:
                print('No jobs selected to delete.')
            else:
                delete_jobs(selected_jobs)

    elif args.subcommand == 'view':
        args.macro = path.abspath(args.macro)
        args.output = path.abspath(args.output)
        if path.isdir(args.output):
            args.output = path.join(args.output, 'geometry.png')
        viewer(macro=args.macro, output=args.output, show=args.show, equal=args.equal)

    elif args.subcommand == 'plot':
        args.data = path.abspath(args.data)
        args.output = path.abspath(args.output)
        if path.isdir(args.output):
            args.output = path.join(args.output, 'histogram.png')
        plotter(args.data, args.hist, output=args.output, norm_factor=args.normFactor, norm_bin=args.normBin)

    elif args.subcommand == 'merge':
        args.folder = path.abspath(args.folder)
        if not path.isdir(args.folder):
            raise NotADirectoryError('No directory with path `%s`' % args.folder)
        merger(args.folder)

    elif args.subcommand == 'see-xs':
        args.output = path.abspath(args.output)
        if path.isdir(args.output):
            args.output = path.join(args.output, 'SEE_XS.png')
        if args.config is not None and 'see-xs' in args.config:
            args.config = args.config['see-xs']
        else:
            args.config = None
        cross_section(config=args.config, output=args.output)
