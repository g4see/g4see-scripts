# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>

"""Recursive histogram merging for parallel G4SEE jobs."""

import os
import subprocess
from glob import glob
from pathlib import Path


def merger(rootdir):
    """Merge output histograms of G4SEE parameterized parallel jobs recursively."""
    app = Path(__file__).parent.parent.absolute().joinpath('mergeHistograms')

    os.chdir(rootdir)
    folder_list = glob('**/*_hist_t*.out', recursive=True)
    folder_list = [Path(*Path(p).parts[:-2]) for p in folder_list]
    folder_list = list(set(folder_list))
    folder_list.sort()
    for folder in folder_list:
        print(folder)
        os.chdir(rootdir)
        os.chdir(folder.absolute())
        subprocess.call([app], stdout=subprocess.DEVNULL)
    if len(folder_list) == 0:
        print('\nNo data found to merge')
    else:
        print('\nAll data have been merged')
