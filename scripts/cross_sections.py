# G4SEE Single Event Effect simulation toolkit
# ============================================
# SPDX-FileCopyrightText: © 2022 CERN for the benefit of the G4SEE Collaboration <https://cern.ch/g4see>
#
# This software is distributed under the terms of the GNU General Public License version 3
# (GPL Version 3) or any later version, copied verbatim in the file "LICENSES/GPL-3.0-or-later.txt".
#
# In applying this license, CERN does not waive the privileges and immunities granted to it
# by virtue of its status as an Intergovernmental Organization or submit itself to any jurisdiction.
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author: Dávid Lucsányi (CERN) <david.lucsanyi@cern.ch>

"""Calculating and plotting SEE cross-sections based on G4SEE Edep histograms."""

import pandas as pd                 # type: ignore
import numpy as np                  # type: ignore
import matplotlib.pyplot as plt     # type: ignore
from plot_histograms import read_g4see_histogram


def energy2electron(x, e=3.6):
    """Convert energy [MeV] to electron [ke-]."""
    return x * 1.e+6 / e / 1000


def electron2energy(x, e=3.6):
    """Convert electron [ke-] to energy [MeV]."""
    return x * 1000 * e / 1.e+6


def energy2charge(x, c=0.022469):
    """Convert energy [MeV] to charge [fC]."""
    return x / c


def charge2energy(x, c=0.022469):
    """Convert charge [fC] to energy [MeV]."""
    return x * c


def read_all_data(cfg):
    """Read all G4SEE datafiles into Pandas DataFrames."""
    df_dict, meta_dict = {}, {}
    for i, data_dict in enumerate(cfg['data']):
        data, uf, of = read_g4see_histogram(data_dict['path'])
        data.dropna(inplace=True)
        df_dict[i] = data
        if 'primary_number' not in data_dict:
            data_dict['primary_number'] = cfg['primary_number']
        data_dict['primary_number'] = float(data_dict['primary_number'])
        if 'beam_area' not in data_dict:
            data_dict['beam_area'] = cfg['beam_area']
        data_dict['beam_area'] = float(data_dict['beam_area'])
        meta_dict[i] = data_dict
    return df_dict, meta_dict


def inverse_cumulative_sum(df_dict, energy_charge_factor=0.022469):
    """Calculate inverse integral of the energy deposition histogram.

    The energy_charge_factor variable [MeV/fC] is used for energy to charge conversion.
    The default 0.022469 MeV/fC value is for Silicon material.
    """
    inv_cum_sum = {}
    for i, df in df_dict.items():
        inv_cum_sum[i] = pd.DataFrame(
            {
                'Edep': df['Edep'].values,
                'Qcrit': df['Edep'].values / energy_charge_factor,
                'InvCum':  np.cumsum(df['Counts'].values[::-1])[::-1]
            }, columns=['Edep', 'Qcrit', 'InvCum'])
    return inv_cum_sum


def plot_xs_qcrit(inv_cum_dict, meta_dict, output_file, cfg):
    """Plot SEE cross-section."""
    plt.rcParams.update({'font.size': cfg['xs_plot']['fontsize']})
    plt.figure(figsize=cfg['xs_plot']['figsize'])

    for i, xs_df in inv_cum_dict.items():
        xs_df['XS'] = xs_df['InvCum'] / meta_dict[i]['primary_number'] * meta_dict[i]['beam_area']
        xs_df = xs_df[(xs_df['XS'] != 0)]
        plt.plot(xs_df['Qcrit'], xs_df['XS'], '.-', label=meta_dict[i]['label'], **cfg['xs_plot']['plot_kwargs'])

        # the XS value closest to critical_charge
        meta_dict[i]['XS'] = xs_df.iloc[(xs_df['Qcrit'] - cfg['critical_charge']).abs().argsort()[0]]['XS']

    # Plot settings
    plt.title(cfg['xs_plot']['title'] + '\n')
    plt.xscale(cfg['xs_plot']['xscale'])
    plt.yscale(cfg['xs_plot']['yscale'])
    plt.xlim(cfg['xs_plot']['xlim'])
    plt.ylim(cfg['xs_plot']['ylim'])
    plt.ylabel(cfg['xs_plot']['ylabel'])
    plt.xlabel(cfg['xs_plot']['xlabel'])
    # Draw critical charge threshold line
    if cfg['xs_plot']['Q_crit']:
        plt.plot([cfg['critical_charge']] * 2, plt.gca().get_ylim(), '--', color='gray', linewidth=2, alpha=0.7,
                 label='Q$_{crit}=%.2f$ fC' % cfg['critical_charge'])
    if cfg['xs_plot']['legend']:
        plt.legend(loc=cfg['xs_plot']['legend_loc'])
    if cfg['xs_plot']['grid']:
        plt.grid(alpha=cfg['xs_plot']['grid_alpha'])
    if cfg['xs_plot']['secondary_xaxis']:
        secax = plt.gca().secondary_xaxis('top', functions=(charge2energy, energy2charge))
        secax.set_xlabel(r'E$_{deposited}$ [MeV]')
    plt.tight_layout()
    plt.savefig(output_file)

    return meta_dict


def cross_section(config, output):
    """Calculate and plot SEE cross-sections."""
    if 'xs_plot' not in config or config['xs_plot'] is None:
        config['xs_plot'] = {}
    plt_cfg = config['xs_plot']
    cfg_defaults = {
        'title':            r'G4SEE | SEE cross-section as function of critical charge',
        'xscale':           'log',
        'yscale':           'log',
        'ylabel':           r'$\sigma_{SEE}$ [cm$^2$/bit]',
        'xlabel':           r'Q$_{crit}$ [fC]',
        'xlim':             [None, None],
        'ylim':             [None, None],
        'legend':           True,
        'legend_loc':       'lower left',
        'grid':             True,
        'grid_alpha':       0.4,
        'Q_crit':           True,
        'plot_kwargs':      {},
        'secondary_xaxis':  True,
        'figsize':          (12, 7),
        'fontsize':         14,
    }
    for k, v in cfg_defaults.items():
        if k not in plt_cfg or plt_cfg[k] is None:
            plt_cfg[k] = v
        elif k in ['xlim', 'ylim']:
            try:
                plt_cfg[k][0] = float(plt_cfg[k][0])
            except ValueError:
                plt_cfg[k][0] = eval(plt_cfg[k][0].capitalize())
            try:
                plt_cfg[k][1] = float(plt_cfg[k][1])
            except ValueError:
                plt_cfg[k][1] = eval(plt_cfg[k][1].capitalize())

    config['critical_charge'] = float(config['critical_charge'])
    raw_data, meta_data = read_all_data(config)
    inv_cum_data = inverse_cumulative_sum(raw_data, energy_charge_factor=config['conversion_factor'])
    meta_data = plot_xs_qcrit(inv_cum_data, meta_data, output_file=output, cfg=config)

    print('SEE cross-sections:')
    print('-------------------')
    for k, v in meta_data.items():
        print('%2d  %14s  %15.3e cm^2/bit' % (k, v['label'], v['XS']))

    plt.show()
