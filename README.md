<!--
SPDX-FileCopyrightText: © 2022 CERN
SPDX-License-Identifier: CC0-1.0
-->

# G4SEE Scripts

Pre- and post-processing scripts of the G4SEE toolkit.

![logo](https://gitlab.cern.ch/g4see/g4see/-/raw/master/doc/source/_static/logos/G4SEE-logo-full.png)

## [Website](https://cern.ch/g4see)

## [Documentation](https://g4see-docs.web.cern.ch/)

## [Changelog](https://gitlab.cern.ch/g4see/g4see/-/blob/master/CHANGELOG.md)

## [Contribution guide](https://gitlab.cern.ch/g4see/g4see/-/blob/master/CONTRIBUTING.md)

## [Copyright](https://gitlab.cern.ch/g4see/g4see/-/blob/master/COPYRIGHT.md)

## [Licenses](LICENSES)

## [Authors](https://gitlab.cern.ch/g4see/g4see/-/blob/master/AUTHORS.md)
